# st-saltmine

My build of the Suckless simple terminal.

Source:

- [https://st.suckless.org/](https://st.suckless.org/)
- [https://dl.suckless.org/st/st-0.8.4.tar.gz](https://dl.suckless.org/st/st-0.8.4.tar.gz)

Patches applied:

- alpha [https://st.suckless.org/patches/alpha/](https://st.suckless.org/patches/alpha/)
- scrollback [https://st.suckless.org/patches/scrollback/](https://st.suckless.org/patches/scrollback/)
